
INSERT INTO ${schema.name}.employee (id, name, address1, address2, city)
   VALUES(1, 'Nathan', '5 State St.', '', 'Minneapolis');
INSERT INTO ${schema.name}.employee (id, name, address1, address2, city)
   VALUES(2, 'Adeel', '201 Park Ave.', '', 'New York');
INSERT INTO ${schema.name}.employee (id, name, address1, address2, city)
   VALUES(3, 'Annette', '85 Lincoln Blvd.', '', 'Austin');
INSERT INTO ${schema.name}.employee (id, name, address1, address2, city)
   VALUES(4, 'Lelsey', '8981 Commonwealth Ave.', '', 'Boston');
